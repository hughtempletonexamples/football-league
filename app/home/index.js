(function () {
  "use strict";

  require("./home.module");

  require("./home.controller");
  
  require("./home.service");

  require("./config.route");

})();
