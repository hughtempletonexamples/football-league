(function () {
  "use strict";

  angular
    .module("footballLeague.home")
    .controller("HomeController", HomeController);

  /**
   * @ngInject
   */
  function HomeController(homeService) {
    var vm = this;

    vm.updateTeam = updateTeam;
    vm.mainCalculate = mainCalculate;
    
    var started = false;
    var complete = true;

    ////////////
    
    init();

    function init(){
      homeService.getTeams()
        .$loaded()
        .then(function (teamsList) {
            vm.teams = teamsList;
        });
    }
    
    function updateTeam(teamName) {
      if(!started){
      vm.newTeam = "";
      return homeService.updateTeam(teamName);
      }
    }

    function mainCalculate() {
      if(complete){
        complete = false;
        started = true;
        return homeService.mainCalculate()
          .then(function () {
            complete = true;
          });
      }
    }
  }
})();
