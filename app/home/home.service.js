(function () {
  "use strict";

  angular
    .module("footballLeague.home")
    .factory("homeService", homeService);

  /**
   * @ngInject
   */
  function homeService(schema, $q) {
    
    var teamsList = getTeams();
    var gamesList = getGames();
    
    var service = {
      mainCalculate: mainCalculate,
      updateTeam: updateTeam,
      getTeams: getTeams
      
    };

    return service;

    ////////////
    
    function updateTeam(teamName, teamData) {
      var existing;
      if (teamName) {
        angular.forEach(teamsList, function (team) {
          if (team.name === teamName) {
            existing = team;
          }
        });

        if (existing) {
          angular.merge(existing, teamData);
          return teamsList.$save(existing);
        } else {
          var data = {"name": teamName, "played": 0, "win": 0, "lose": 0, "home": 0, "away": 0, "draw": 0, "goals": 0, "goalsAgainst": 0, "gd": 0, "points": 0};
          var newDatumData = angular.extend({}, data);
          return teamsList.$add(newDatumData);  
        }
      }
    }

    function mainCalculate() {
      
      var deferred = $q.defer();
      
        teamsList
          .$loaded()
          .then(teamsToPlay)
          .then(calculateStatstics)
          .then(function () {
            deferred.resolve();
          });
          
      return deferred.promise;
    }

    function teamsToPlay() {
      var promises = [];
      var teamsPlayed = [];
      angular.forEach(teamsList, function (team) {
        var keepGoing = true;
        angular.forEach(teamsList, function (team2) {
          var team1Played = teamsPlayed.indexOf(team.name);
          var team2Played = teamsPlayed.indexOf(team2.name);
          if (team.$id !== team2.$id && team1Played === -1 && team2Played === -1) {
            if (angular.isUndefined(team.homeListing) || angular.isUndefined(team.awayListing)) {
              //play game home or away
              if (angular.isUndefined(team.homeListing)) {
                //play game home
                if (keepGoing) {
                  teamsPlayed.push(team.name);
                  teamsPlayed.push(team2.name);
                  promises.push(playGame(team, team2, "home"));
                  keepGoing = false;
                }
              }
              if (angular.isUndefined(team.awayListing)) {
                //play game away
                if (keepGoing) {
                  teamsPlayed.push(team.name);
                  teamsPlayed.push(team2.name);
                  promises.push(playGame(team, team2, "away"));
                  keepGoing = false;
                }
              }
            } else {
              if (angular.isDefined(team.homeListing) && angular.isUndefined(team.homeListing[team2.$id])) {
                //play game home
                if (keepGoing) {
                  teamsPlayed.push(team.name);
                  teamsPlayed.push(team2.name);
                  promises.push(playGame(team, team2, "home"));
                  keepGoing = false;
                }
              }
              if (angular.isDefined(team.awayListing) && angular.isUndefined(team.awayListing[team2.$id])) {
                //play game away
                if (keepGoing) {
                  teamsPlayed.push(team.name);
                  teamsPlayed.push(team2.name);
                  promises.push(playGame(team, team2, "away"));
                  keepGoing = false;
                }
              }
            }
          }
        });
      });
      return $q.all(promises);
    }

    function playGame(team, team2, ground) {

      var teamData = {};
      var team2Data = {};

      var data = {
        "result": {},
        "ground": ""
      };

      data.result[team.$id] = Math.floor(Math.random() * 10);
      data.result[team2.$id] = Math.floor(Math.random() * 10);

      if (ground === "home") {
        if (angular.isUndefined(teamData["homeListing"])) {
          teamData["homeListing"] = {};
        }
        if (angular.isUndefined(teamData["homeListing"][team2.$id])) {
          teamData["homeListing"][team2.$id] = true;
        }
        if (angular.isUndefined(team2Data["awayListing"])) {
          team2Data["awayListing"] = {};
        }
        if (angular.isUndefined(team2Data["awayListing"][team.$id])) {
          team2Data["awayListing"][team.$id] = true;
        }
        data.ground = team.$id;
      } else {
        if (angular.isUndefined(teamData["awayListing"])) {
          teamData["awayListing"] = {};
        }
        if (angular.isUndefined(teamData["awayListing"][team2.$id])) {
          teamData["awayListing"][team2.$id] = true;
        }
        if (angular.isUndefined(team2Data["homeListing"])) {
          team2Data["homeListing"] = {};
        }
        if (angular.isUndefined(team2Data["homeListing"][team.$id])) {
          team2Data["homeListing"][team.$id] = true;
        }
        data.ground = team2.$id;
      }

      return addGame(data)
        .then(function () {
          return updateTeam(team.name, teamData);
        })
        .then(function () {
          return updateTeam(team2.name, team2Data);
        });
    }

    function addGame(data) {
      var newGameData = angular.extend({}, data);
      return gamesList.$add(newGameData);
    }

    function calculateStatstics() {
      var promises = [];
      angular.forEach(teamsList, function (team) {
        promises.push(indevidualStatistics(team));
      });
      return $q.all(promises);
    }

    function indevidualStatistics(team) {

      var data = {"played": 0, "win": 0, "lose": 0, "home": 0, "away": 0, "draw": 0, "goals": 0, "goalsAgainst": 0, "gd": 0, "points": 0};

      angular.forEach(gamesList, function (game) {
        //see how this can be used!!!
        if (angular.isDefined(game.result[team.$id])) {
          var goals;
          var goalsAgainst;
          data.played++;
          if (game.ground === team.$id) {
            data.home++;
          } else {
            data.away++;
          }
          angular.forEach(game.result, function (result, key) {
            if (team.$id === key) {
              goals = result;
              data.goals += result;
            }
            if (team.$id !== key) {
              goalsAgainst = result;
              data.goalsAgainst += result;
            }
          });
          if (goals > goalsAgainst) {
            data.win++;
            data.points += 3;
          } else if (goals < goalsAgainst) {
            data.lose++;
          } else if (goals === goalsAgainst) {
            data.points++;
            data.draw++;
          }
        }
      });
      data.gd = data.goals - data.goalsAgainst;
      return updateTeam(team.name, data);
    }

    function getTeams() {
      var teamsRef = schema.getRoot().child("teams");
      return schema.getArray(teamsRef);
    }

      function getGames() {
      var gamesRef = schema.getRoot().child("games");
      return schema.getArray(gamesRef);
    }

  }

})();