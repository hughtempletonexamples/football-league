(function () {
  "use strict";

  angular
    .module("footballLeague.home", [
      "footballLeague.core"
    ]);

})();
