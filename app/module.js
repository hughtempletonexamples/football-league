(function () {
  "use strict";

  angular.module("footballLeague", [
    "ngRoute",
    "ngAnimate",
    "ngTouch",
    "ngMessages",
    "ui.bootstrap",
    
    "footballLeague.core",
    "footballLeague.home"
  ]);
})();
