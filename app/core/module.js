(function () {
  "use strict";

  angular.module(
    "footballLeague.core",
    [
      "firebase.database",
      "firebase.auth"
    ]);
})();
