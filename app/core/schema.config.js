/* global PRODUCTION:false FIREBASE:false FIREBASEAPIKEY:false */
(function () {
  "use strict";
  angular
    .module("footballLeague.core")
    .run(function ($window, $location) {

      var config = {
        apiKey: "AIzaSyCgO9NTsG9kESaf4U3lrZKNli9nIit8t4w",
        authDomain: "functions-test-4b6dd.firebaseapp.com",
        databaseURL: "https://functions-test-4b6dd.firebaseio.com",
        projectId: "functions-test-4b6dd",
        storageBucket: "functions-test-4b6dd.appspot.com",
        messagingSenderId: "784790273150"
      };

      // Initialize Firebase connection
      $window.firebase.initializeApp(config);
    });
})();

