(function () {
  "use strict";

  angular
    .module("footballLeague")
    .config(config);

  /**
   * @ngInject
   */
  function config($routeProvider) {
    $routeProvider.otherwise({redirectTo: "/home"});
  }
})();
